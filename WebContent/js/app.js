/**
 * 
 */
var app = angular.module('ngTrain', [ 'ngRoute','ngResource' ]);

/** Configuration * */

app.config([ '$routeProvider', function($routeProvider) {
	
	
	$routeProvider.when('/home', {
		templateUrl : 'js/components/landing/homeView.html'
	}).when('/about', {
		templateUrl : 'js/components/about/aboutView.html'
	}).when('/online_training', {
		templateUrl : 'js/components/services/onlineTrainingView.html'
	}).when('/corporate_training', {
		templateUrl : 'js/components/services/corpTrainingView.html'
	}).when('/app_development', {
		templateUrl : 'js/components/services/appDevelopView.html'
	}).when('/job_support', {
		templateUrl : 'js/components/services/jobSupportView.html'
	}).when('/offered_courses', {
		templateUrl : 'js/components/courses/coursesView.html'
	}).when('/offered_courses', {
		templateUrl : 'js/components/courses/coursesView.html'
	}).when('/online_training_student_register', {
		templateUrl : 'js/components/register/regStudentView.html'
	}).when('/online_trainer_register', {
		templateUrl : 'js/components/register/regTrainerView.html'
	}).when('/contact', {
		templateUrl : 'js/components/contact/contactView.html'
	}).otherwise({
		redirectTo : '/home'
	});
} ]);



