/**
 * Header directive
 */
app.directive('header', function() {
	return {
		restrict : 'A',
		templateUrl : "js/shared/headerView.html",
		link : function(scope, elem, attrs) {
			init();
			
		},
		controller : function() {
		},
		controllerAs : "hd"
	}
});

/**
 * Footer Directive
 */
app.directive('footer', function() {
	return {
		restrict : 'A',
		templateUrl : "js/shared/footerView.html"
	}
});


app.directive('flexslider', [ function() {
	return {
		restrict : 'A',
		link : function(scope, elem, attrs) {
			$(elem).flexslider({
				animation : "fade",
				touch : true,
				controlNav : false,
				prevText : "&nbsp;",
				nextText : "&nbsp;"
			});
		}
	};
} ]);


app.directive('upcomeBatch', function(){
    return {
      restrict: 'E',
      scope: false,
      templateUrl: 'js/shared/upcomeBatchView.html',
      controller : "upCoursesController",
      controllerAs: 'upCrs'
    }
  });