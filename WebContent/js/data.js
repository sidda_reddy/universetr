/**
 * 
 */

var courses = [ {
	id : 1,
	name : "JAVA",
	grid : {
		show : true,
		order : 1,
		title : "Core & Advance Java",
		shortDesc : "Featuring painting, sculpture, ceramics",
		rating : 5,
		img : "images/course/java_j2se_online_training.jpg"
	},
	desc : ""
}, {
	id : 2,
	name : "Tableau",
	grid : {
		show : true,
		order : 1,
		title : "Core & Advance Java",
		shortDesc : "Featuring painting, sculpture, ceramics",
		rating : 5,
		img : "images/course/java_j2se_online_training.jpg"
	},
	desc : ""
}, {
	id : 3,
	name : "SQL BI",
	grid : {
		show : true,
		order : 1,
		title : "Core & Advance Java",
		shortDesc : "Featuring painting, sculpture, ceramics",
		rating : 5,
		img : "images/course/java_j2se_online_training.jpg"
	},
	desc : ""
}, {
	id : 4,
	name : ".Net",
	grid : {
		show : true,
		order : 1,
		title : "Core & Advance Java",
		shortDesc : "Featuring painting, sculpture, ceramics",
		rating : 5,
		img : "images/course/java_j2se_online_training.jpg"
	},
	desc : ""
}, {
	id : 5,
	name : "AGULAR JS",
	grid : {
		show : true,
		order : 1,
		title : "Core & Advance Java",
		shortDesc : "Featuring painting, sculpture, ceramics",
		rating : 5,
		img : "images/course/java_j2se_online_training.jpg"
	},
	desc : ""
} ];

var services = [
		{
			id : 1,
			name : "Online Training",
			desc : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum erat et neque tincidunt volutpat. Cras eget augue id dui varius pretium."
		},
		{
			id : 2,
			name : "Corporate Training",
			desc : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum erat et neque tincidunt volutpat. Cras eget augue id dui varius pretium."
		},
		{
			id : 3,
			name : "Software Development",
			desc : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum erat et neque tincidunt volutpat. Cras eget augue id dui varius pretium."
		},
		{
			id : 4,
			name : "Job Support",
			desc : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum erat et neque tincidunt volutpat. Cras eget augue id dui varius pretium."
		}, ];