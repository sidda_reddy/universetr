/**
 * Slider for home page
 */
app.directive('pslider', [ function() {
	return {
		restrict : 'A',
		link : function(scope, elem, attrs) {
			$(elem).pSlider({
		        slider: $('#slider-testimonials ul li'),
		        visible: 1,
		        button: {
		            next: $('#slider-testimonials .next'),
		            prev: $('#slider-testimonials .prev')
		        }
		    });
		}
	};
} ]);

/**
 * Google map directive for contact page
 */

app.directive('googleMap',  [ function () {
  return {
    restrict: 'A',
    link : function(scope, elem, attrs) {
    	 var script = document.createElement('script');
         script.type = 'text/javascript';
         script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
             'callback=initialize';
         document.body.appendChild(script);
    }
  };
}]);

/** Courses page accordion **/

app.directive('onFinishRender', function () {
    return {
        restrict: 'A',        
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                element.ready(function () {                    
                    scope.accordion();
                });
            }
        }
    }
});
