/** Home Controller **/

app.controller("homeController", ['$scope','Courses', function($scope, Courses){
	$scope.demo = {};
	this.courses = Courses.query();
	
}])

/** Demo form Controllers **/

app.controller("requestDemo", ['$scope', function($scope){
	$scope.demo = {};
	$scope.submit = function(demo) {
		//JSON.stringify(demo);
	};
}]);

/** Upcoming courses **/
app.controller("upCoursesController", ['UpCourses', function(UpCourses){
	this.courses = UpCourses.query();
}]);

/** Course details controller * */
app.controller("courseDetails", [ 'CourseDetails', '$scope',
		function(CourseDetails, $scope) {
			
			this.courses = CourseDetails.query();
			this.page = 1.1;
			$scope.accordion = function() {
				
				$h3 = $("#accordion h3");
				$h3.each(function() {
					if($(this).parent().is( "ng-repeat" )){
						$(this).unwrap();
					};
				});
				$("#accordion").accordion({
					heightStyle : "content",
					activate : function() {
						$("#accordion h3").removeClass('ui-corner-all');
					}
				});
				$("#accordion h3").removeClass('ui-corner-all');
				$(".ui-accordion .ui-accordion-header").css({
					"padding-top" : "8px",
					"padding-bottom" : "7px",
					"font-weight" : "600"
				});
			}
		} ]);

