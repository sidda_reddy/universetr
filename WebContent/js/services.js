/**
 * Service for getting courses data
 */

app.factory('Courses', [ '$resource', function($resource) {
	return $resource('data/courses.json', {}, {
        query: {
          method: 'GET',
          isArray: true
        }
	})
} ]);

/** Upcoming courses **/

app.factory('UpCourses', [ '$resource', function($resource) {
	return $resource('data/upcourses.json', {}, {
        query: {
          method: 'GET',
          isArray: true
        }
	})
} ]);

/** Course details page **/

app.factory('CourseDetails', [ '$resource', function($resource) {
	return $resource('data/coursedetails.json?1', {}, {
        query: {
          method: 'GET',
          isArray: true
        }
	})
} ]);